/**************************************************/
//player class
var Player = function (id, name, color, image, startX, startY) {
	var id = id,
		name = name,
		color = color,
		image = image,
		x = startX,
		y = startY

	// Getters and setters

	var getId = function () {
		return id;
	};

	var getName = function () {
		return name;
	};

	var getColor = function () {
		return color;
	};

	var getImage = function () {
		return image;
	};

	var getX = function () {
		return x;
	};

	var getY = function () {
		return y;
	};

	var setX = function (newX) {
		x = newX;
	};

	var setY = function (newY) {
		y = newY;
	};

	// Define which variables and methods can be accessed
	return {
		getId: getId,
		getName: getName,
		getColor: getColor,
		getImage: getImage,
		getX: getX,
		getY: getY,
		setX: setX,
		setY: setY
	}
};

// Export the Player class 
exports.Player = Player;