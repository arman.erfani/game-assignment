var Treasure = (function () {


    this.anchorX;
    this.anchorY;
    this.speed;
    this.direction;
    this.motionRange

    function Treasure(anchorX, anchorY, x, y, color, image, visible) {
        GameSprite.call(this, x, y, 46, 46, color, image, visible);
        this.anchorX = anchorX;
        this.anchorY = anchorY;
        this.speed = {
            x: 0.1,
            y: 0.1
        };
        this.direction = -1;
        this.motionRange = 3

    }


    Treasure.prototype = Object.create(GameSprite.prototype);
    Treasure.prototype.constructor = Treasure;

    Treasure.prototype.update = function (keys) {
        this.y -= this.direction * this.speed.y;
        if (this.y - this.anchorY >= this.motionRange || this.anchorY - this.y >= this.motionRange) {
            this.direction *= -1;
        }
    }
    return Treasure;
})();