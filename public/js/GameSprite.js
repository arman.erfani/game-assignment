var GameSprite = (function () {
    this.x;
    this.y;
    this.width;
    this.height;
    this.color;
    this.image;
    this.visible;

    function GameSprite(x, y, width, height, color, image, visible) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.color = color;
        this.image = image;
        this.visible = visible;
    }

    GameSprite.prototype =
        {
            update: function () {
                throw new Error("AbstractMethod is not Implemented");
            },
            render: function (keys) {
                if (this.visible == true) {
                    //ctx.beginPath();
                    //ctx.rect(x, y, width, width);
                    //ctx.fillStyle = color;
                    //ctx.fill();

                    if (keys.up) {

                    } else if (keys.down) {

                    };

                    if (keys.left) {

                    } else if (keys.right) {

                    };

                    var img = new Image();
                    img.src = this.image;
                    //img.className  = 'rotateimg180';

                    //ctx.save();
                    //ctx.translate(0, 0)
                    //ctx.rotate(180 * Math.PI / 180);
                    //var y2 = y * Math.cos(180 * Math.PI / 180) - x * Math.sin(180 * Math.PI / 180)
                    //var x2 = y * Math.sin(180 * Math.PI / 180) + x * Math.cos(180 * Math.PI / 180)

                    //ctx.drawImage(img, this.x, this.y, img.width, img.height, this.x, this.y, this.width, this.height);
                    ctx.drawImage(img, this.x, this.y);
                    ctx.restore();

                }
            },
            isCollided: function (target, offset) {
                if (this.x < target.x + target.width - offset &&
                    this.x + this.width - offset > target.x &&
                    this.y < target.y + target.height - offset &&
                    this.height - offset + this.y > target.y) {
                    return true;
                }
                return false;
            }
        }

    return GameSprite;
})();