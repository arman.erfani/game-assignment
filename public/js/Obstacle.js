var Obstacle = (function () {


    this.anchorX;
    this.anchorY;
    this.speed;
    this.direction;
    this.motionRange

    function Obstacle(anchorX, anchorY, x, y, color, image, visible, direction) {
        GameSprite.call(this, x, y, 46, 46, color, image, visible);
        this.anchorX = anchorX;
        this.anchorY = anchorY;
        this.speed = {
            x: 1,
            y: 1
        };
        this.direction = direction;
        this.motionRange = 40

    }


    Obstacle.prototype = Object.create(GameSprite.prototype);
    Obstacle.prototype.constructor = Player;

    Obstacle.prototype.update = function (keys) {
        this.y -= this.direction * this.speed.y;
        if (this.y - this.anchorY == this.motionRange || this.anchorY - this.y == this.motionRange) {
            this.direction *= -1;
        }


    }
    return Obstacle;
})();