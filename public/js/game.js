/**************************************************/
// game global variables
var canvas,
	seaLevel,
	ctx,
	keys,
	localPlayer,
	remotePlayers,
	obstacles = [],
	treasures = [],
	gameState = 1,
	gameMode,
	socket;		// socket.io 	

/**************************************************/
// initiation
function init() {
	gameState = 1;
	// Declare the canvas and rendering context
	canvas = document.getElementById("gameCanvas");
	ctx = canvas.getContext("2d");

	window.innerWidth = 900;
	window.innerHeight = 500;
	seaLevel = 400;
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;

	// init keyboard input
	keys = new Keys();

	obstacles = [];
	obstacles.push(new Obstacle(50, 200, 50, 200, 'red', 'style/img/buffer_fish.png', true, -1));
	obstacles.push(new Obstacle(800, 200, 800, 200, 'red', 'style/img/buffer_fish.png', true, -1));

	obstacles.push(new Obstacle(300, 150, 300, 150, 'red', 'style/img/buffer_fish.png', true, -1));
	obstacles.push(new Obstacle(550, 150, 550, 150, 'red', 'style/img/buffer_fish.png', true, -1));

	obstacles.push(new Obstacle(150, 300, 150, 300, 'red', 'style/img/buffer_fish.png', true, 1));
	obstacles.push(new Obstacle(700, 300, 700, 300, 'red', 'style/img/buffer_fish.png', true, 1));

	obstacles.push(new Obstacle(250, 400, 250, 400, 'red', 'style/img/buffer_fish.png', true, -1));
	obstacles.push(new Obstacle(600, 400, 600, 400, 'red', 'style/img/buffer_fish.png', true, -1));

	treasures = [];
	treasures.push(new Treasure(0, 200, 0, 200, 'red', 'style/img/treasure.png', true));
	treasures.push(new Treasure(850, 200, 850, 200, 'red', 'style/img/treasure.png', true));

	treasures.push(new Treasure(300, 100, 300, 100, 'red', 'style/img/treasure.png', true));
	treasures.push(new Treasure(550, 100, 550, 100, 'red', 'style/img/treasure.png', true));

	treasures.push(new Treasure(50, 400, 50, 400, 'red', 'style/img/treasure.png', true));
	treasures.push(new Treasure(800, 400, 800, 400, 'red', 'style/img/treasure.png', true));
	//treasures.push(new Treasure(700, 200, 0, 200, 'red', 'style/img/treasure.png', true));
	//treasures.push(new Treasure(600, 400, 600, 400, 'red', 'style/img/treasure.png', true));
	//treasures.push(new Treasure(400, 100, 0, 200, 'red', 'style/img/treasure.png', true));
	//treasures.push(new Treasure(700, 500, 600, 400, 'red', 'style/img/treasure.png', true));

	// init socket
	socket = io('/');
	// init remote players 
	remotePlayers = [];

	gameMode = 'single-player';
	// set  event handlers
	setEventHandlers();
};


/**************************************************/
// game event handlers
var setEventHandlers = function () {
	// key controls
	window.addEventListener("keydown", onKeydown, false);
	window.addEventListener("keyup", onKeyup, false);

	socket.on("connect", onSocketConnected);

	socket.on("player initialised", onPlayerInitialised);
};

function onKeydown(e) {
	if (localPlayer) {
		keys.onKeyDown(e);
	};
};

function onKeyup(e) {
	if (localPlayer) {
		keys.onKeyUp(e);
	};
};

function onSocketConnected() {
	console.log("new player connected to server");
	// emite new player info to server
	socket.emit("initialise player", { name: document.getElementById('playerName').value, x: 0, y: 0 });
};

function onPlayerInitialised(data) {
	console.log("new player connected to server");
	//create local player for the first and last time
	localPlayer = new Player(data.id, data.name, data.color, data.image, true);

	// a remote player created 
	socket.on("new player", onNewPlayer);

	socket.on("disconnect", onSocketDisconnect);

	// player position changed
	socket.on("move player", onMovePlayer);

	// remove remote player from list
	socket.on("remove player", onRemovePlayer);

	// emit newly created local  player info to server
	socket.emit("new player", { id: localPlayer.id, name: localPlayer.name, color: localPlayer.color, image: localPlayer.image, x: 0, y: 0 });
};

function onSocketDisconnect() {
	console.log("Disconnected from socket server");
};

function onNewPlayer(data) {
	console.log("new remote player connected to server with id: " + data.id);
	var newPlayer = new Player(data.id, data.name, data.color, data.image, true);
	remotePlayers.push(newPlayer);
	gameMode = 'multi-player';
};

function onMovePlayer(data) {
	var movePlayer = playerById(data.id);

	// player not exists
	if (!movePlayer) {
		console.log("Player not found: " + data.id);
		return;
	};

	// set remote player new position from server values
	movePlayer.x = data.x;
	movePlayer.y = data.y;
};

function onRemovePlayer(data) {
	var removePlayer = playerById(data.id);

	// Player not found
	if (!removePlayer) {
		console.log("player not exists: " + data.id);
		return;
	};

	// remove remote player from list
	remotePlayers.splice(remotePlayers.indexOf(removePlayer), 1);
	if (remotePlayers.length == 0) {
		gameMode = 'single-player';
	}
};


//****************************/
// animation loop
function animate() {
	update();
	render();

	//Paul Irish's shim animation frame
	if (gameState == 1)
		window.requestAnimationFrame(animate);
};

//****************************/
// stop the game
function stop() {
	setTimeout(function () {
		// disconect from server
		socket.disconnect();

		//stop the music
		var sound = document.getElementById('main_audio');
		sound.pause();
		sound.currentTime = 0;

		location.reload();
	}, 5000);

};

//****************************/
// update game scene
function update() {
	// if game is finished or not started yet there is no need to change anything
	if (gameState == 0)
		return;

	// update game obstacles
	for (i = 0; i < obstacles.length; i++) {
		obstacles[i].update(keys);
	};

	// update game treasures
	for (i = 0; i < treasures.length; i++) {
		treasures[i].update(keys);
	};

	// update local player and check for change
	if (localPlayer && localPlayer.update(keys)) {
		// emit local player data to server if position is changed
		socket.emit("move player", { x: localPlayer.x, y: localPlayer.y });
	};

	// check if any of remote players collided with game objects
	var i;
	for (i = 0; i < remotePlayers.length; i++) {
		remotePlayers[i].checkCollisions();
	};
};

// render game scene
function render() {
	// reset  the canvas
	ctx.clearRect(0, 0, canvas.width, canvas.height);

	renderCanvasBackground();

	if (gameState == 0) {
		renderResult();
	}
	// render the local player
	if (localPlayer) {
		renderScoreBoard();
		localPlayer.render(keys);
	}

	// render the remote players
	var i;
	for (i = 0; i < remotePlayers.length; i++) {
		remotePlayers[i].render(keys);
	};

	// render treasures
	for (i = 0; i < treasures.length; i++) {
		treasures[i].render(keys);
	};

	// render obstacles
	for (i = 0; i < obstacles.length; i++) {
		obstacles[i].render(keys);
	};


};

function renderCanvasBackground() {
	//sky
	ctx.fillStyle = "#bbd2f7";
	ctx.fillRect(0, 0, canvas.width, canvas.height - seaLevel);

	//water
	ctx.fillStyle = "#2775f4";
	ctx.fillRect(0, canvas.height - seaLevel, canvas.width, seaLevel);
}
function renderResult() {


	ctx.fillStyle = "yellow";
	ctx.fillRect(canvas.width / 2 - 100, canvas.height / 2 - 50, 200, 50);

	if (localPlayer.isAlive) {
		if (gameMode == 'single-player') {
			ctx.fillStyle = "green";
			ctx.font = "14pt sans-serif";
			ctx.fillText("You Won!", canvas.width / 2 - 100 + 5, canvas.height / 2 - 25);
		}
		else {
			var maxTreasures = localPlayer.treasureCount,
				winnerIndex = -1;
			// draw font in red
			var i;
			for (i = 0; i < remotePlayers.length && remotePlayers[i].isAlive; i++) {
				if (remotePlayers[i].treasureCount > maxTreasures) {
					maxTreasures = remotePlayers[i].treasureCount;
					winnerIndex = i;
				}
			};

			if (winnerIndex == -1) {
				ctx.fillStyle = "green";
				ctx.font = "14pt sans-serif";
				ctx.fillText("You Won!", canvas.width / 2 - 100 + 5, canvas.height / 2 - 25);
			}
			else {
				ctx.fillStyle = "green";
				ctx.font = "14pt sans-serif";
				ctx.fillText(remotePlayers[winnerIndex].name + " Won!", canvas.width / 2 - 100 + 5, canvas.height / 2 - 25);
			}
		}
	}
	else {
		ctx.fillStyle = "red";
		ctx.font = "14pt sans-serif";
		ctx.fillText("You Lost!", canvas.width / 2 - 100 + 5, canvas.height / 2 - 25);
	}

	stop();

}

function renderScoreBoard() {
	ctx.fillStyle = "yellow";
	ctx.fillRect(0, 0, 400, 20);
	// draw font in red
	ctx.fillStyle = "#000";
	ctx.font = "10pt sans-serif";
	ctx.fillText("Scores-> You: " + localPlayer.treasureCount * 10, 5, 15);
	var i;
	for (i = 0; i < remotePlayers.length; i++) {
		ctx.fillText(', ' + remotePlayers[i].name + ': ' + remotePlayers[i].treasureCount * 10, (i * 5) + 200, 15);
	};
}
/**************************************************/
// find remote player by id
function playerById(id) {
	var i;
	for (i = 0; i < remotePlayers.length; i++) {
		if (remotePlayers[i].id == id)
			return remotePlayers[i];
	};
	return false;
};

