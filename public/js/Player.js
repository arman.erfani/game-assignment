var Player = (function () {
    function Player(id, name, color, image, visible) {
        GameSprite.call(this, canvas.width / 2, canvas.height / 2, 46, 46, color, image, visible);
        this.id = id;
        this.name = name;
        this.speed = {
            x: 2,
            y: 2
        };
        this.treasureCount = 0;
        this.isAlive = true;
    }

    // inherit from GameSprite
    Player.prototype = Object.create(GameSprite.prototype);
    Player.prototype.constructor = Player;
    //player public methods
    Player.prototype.update = function (keys) {
        this.checkCollisions();
        var currentPos = { x: this.x, y: this.y };

            if (keys.up) {
                this.y -= this.speed.y;
                this.setImage(this.image, 'up');
            } else if (keys.down) {
                this.y += this.speed.y;
                this.setImage(this.image, 'down');
            };
            
            if (keys.left) {
                this.x -= this.speed.x;
                this.setImage(this.image, 'left');
            } else if (keys.right) {
                this.x += this.speed.x;
                this.setImage(this.image, 'right');
            };
        


        // Check Collision with Borders
        if (this.y + (this.height / 2) <= canvas.height - seaLevel) {
            this.y += this.speed.y;
        }

        if (this.y + this.height >= canvas.height) {
            this.y -= this.speed.y;
        }

        if (this.x + this.width >= canvas.width) {
            this.x -= this.speed.x;
        }

        if (this.x <= 0) {
            this.x += this.speed.x;
        }

        //check if the object is not moved within the game interval
        return (currentPos.x == this.x && currentPos.y == this.y) ? false : true;
    };
    Player.prototype.setImage = function (image, direction) {
        if (!direction)
            this.image = image;
        else {
            var fileName = image.replace(/^.*[\\\/]/, '');
            var fileBaseName = fileName.split('-')[0];
            var fileExtension = image.split('.').pop();
            var fileDir = image.substring(0, image.lastIndexOf('/'));

            this.image = fileDir + '/' + fileBaseName + '-' + direction + '.' + fileExtension
        }
    };
    Player.prototype.checkCollisions = function () {
        // Check Collision with Borders and Sea level
        if (this.y + (this.height / 2) <= canvas.height - seaLevel) {
            this.y += this.speed.y;
        }

        if (this.y + this.height >= canvas.height) {
            this.y -= this.speed.y;
        }

        if (this.x + this.width >= canvas.width) {
            this.x -= this.speed.x;
        }

        if (this.x <= 0) {
            this.x += this.speed.x;
        }

        // check collision with game obstacles
        if (obstacles.length > 0) {
            for (var i = 0; i < obstacles.length; i++) {
                if (this.isCollided(obstacles[i], 10)) {
                    this.isAlive = false;
                    gameState = 0;
                }
            }
        }

        // check collision with game treasures
        if (treasures.length > 0) {
            for (var i = 0; i < treasures.length; i++) {
                if (this.isCollided(treasures[i], 10) && treasures[i].visible) {
                    ++this.treasureCount;
                    treasures[i].visible = false;

                    var i;
                    var visibleTreasuresCount = 0;
                    for (i = 0; i < treasures.length; i++)
                        if (treasures[i].visible == true)
                            visibleTreasuresCount++;
                    if (visibleTreasuresCount == 0) {
                        gameState = 0;
                    }

                }
            }
        }
    };
    return Player;
})();