
var gulp = require('gulp'),
    gutil = require('gulp-util'),
    connect = require('gulp-connect'),
    minify = require('gulp-minify'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    useref = require('gulp-useref');

var jsSources = ['public/js/*.js'],
    styleSources = ['public/style/**/*'],
    htmlSources = ['public/*.html'],
    outputDir = 'public/dist',
    jsOutputDir = outputDir + '/js',
    styleOutputDir = outputDir + '/style';


gulp.task('log', function () {
    gutil.log('== My First Task ==')
});

gulp.task('copy', function () {
    gulp.src(htmlSources)
        .pipe(useref())
        .on('error', gutil.log)
        .pipe(gulp.dest(outputDir))
});

gulp.task('copyStyle', function () {
    gulp.src(styleSources)
        .pipe(gulp.dest(styleOutputDir))
});

//***** useref does the job :) 
// gulp.task('js', function () {
//     gulp.src(jsSources)
//         .pipe(minify())
//         .pipe(concat('main.js'))
//         .pipe(gulp.dest(jsOutputDir))
//         .pipe(connect.reload())
// });

gulp.task('watch', function () {
    gulp.watch(jsSources, ['js']);
    gulp.watch(htmlSources, ['html']);
    gulp.watch(htmlSources, ['copyStyle']);
    gulp.watch(htmlSources, ['copy']);
});

gulp.task('connect', function () {
    connect.server({
        root: '.',
        livereload: true
    })
});

gulp.task('html', function () {
    gulp.src(htmlSources)
        .pipe(connect.reload())
});

gulp.task('default', ['copy', 'copyStyle', 'connect', 'watch', 'html']);