/**************************************************
** GAME PLAYER CLASS
**************************************************/
var Configs = {
        host: {port:8000},
        player: {
                colors: ['green', 'blue'],
                images: ['style/img/player_1-right.png', 'style/img/player_2-left.png']
        }
}

// Export the Config class
exports.Configs = Configs;