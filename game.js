
// loading dependencies
var util = require("util");
var Player = require("./Player").Player;
var Configs = require("./Configs").Configs;	// Player class
var http = require('http');
var fs = require('fs');


//global variables
var socket,		// Socket controller
	players	// connected clients 

// Loading the index file . html displayed to the client
// var server = http.createServer(function(req, res) {
//     fs.readFile('./public/index.html', 'utf-8', function(error, content) {
//         //res.writeHead(200, {"Content-Type": "text/html"});
//         res.end(content);
//     });
// });

var express = require('express');
var path = require('path');
var app = express();
app.use(express.static(path.join(__dirname, '')));

var server = http.createServer(app);
// load socket.io
var io = require('socket.io').listen(server);
io.set('origins', '*:*');

/***************************************/
// initial game
function init() {
	// Create an empty array to store players
	players = [];

	// listen to port from config
	server.listen(Configs.host.port);

	setEventHandlers();
};


/*****************************************/
// server event handlers
var setEventHandlers = function () {
	// Socket.IO
	io.sockets.on("connection", onSocketConnection);
};

// new socket connection
function onSocketConnection(client) {
	util.log("New player has connected: " + client.id);

	// Listen for client disconnected
	client.on("disconnect", onClientDisconnect);

	// Listen for new player message
	client.on("initialise player", onInitialisePlayer);

	// Listen for new player message
	client.on("new player", onNewPlayer);

	// Listen for move player message
	client.on("move player", onMovePlayer);
};

// Socket client has disconnected
function onClientDisconnect() {
	util.log("Player has disconnected: " + this.id);

	var removePlayer = playerById(this.id);

	// Player not found
	if (!removePlayer) {
		util.log("Player not found: " + this.id);
		return;
	};

	// Remove player from players array
	players.splice(players.indexOf(removePlayer), 1);

	// Broadcast removed player to connected socket clients
	this.broadcast.emit("remove player", { id: this.id });
};

// New player has joined
function onNewPlayer(data) {
	// Create a new player
	var newPlayer = new Player(data.id,
		data.name,
		data.color,
		data.image,
		data.x, data.y);

	// Broadcast new player to connected socket clients
	this.broadcast.emit("new player", {
		id: newPlayer.getId(),
		name: newPlayer.getName(),
		color: newPlayer.getColor(),
		image: newPlayer.getImage(),
		x: newPlayer.getX(), y: newPlayer.getY()
	});

	// Send existing players to the new player
	var i, existingPlayer;
	for (i = 0; i < players.length; i++) {
		existingPlayer = players[i];
		this.emit("new player", {
			id: existingPlayer.getId(),
			name: existingPlayer.getName(),
			color: existingPlayer.getColor(),
			image: existingPlayer.getImage(),
			x: existingPlayer.getX, y: existingPlayer.getY()
		});
	};

	// Add new player to the players array
	players.push(newPlayer);
};


// initialise new player
function onInitialisePlayer(data) {
	// Broadcast new player to connected socket clients
	this.emit("player initialised", {
		id: this.id,
		name:data.name,
		color: Configs.player.colors[players.length % 2],
		image: Configs.player.images[players.length % 2]
	});
};


// Player has moved
function onMovePlayer(data) {
	// Find player in array
	var movePlayer = playerById(this.id);

	// Player not found
	if (!movePlayer) {
		util.log("Player not found: " + this.id);
		return;
	};

	// Update player position
	movePlayer.setX(data.x);
	movePlayer.setY(data.y);
	// Broadcast updated position to connected socket clients
	this.broadcast.emit("move player", { id: movePlayer.getId(), x: movePlayer.getX(), y: movePlayer.getY() });
};

//helper functions
function playerById(id) {
	var i;
	for (i = 0; i < players.length; i++) {
		if (players[i].getId() == id)
			return players[i];
	};

	return false;
};

//start the game server
init();
